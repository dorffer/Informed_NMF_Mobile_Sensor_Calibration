# Informed Nonnegative Matrix Factorization Methods for Mobile Sensor Network Calibration
## by Clément Dorffer, Matthieu Puigt, Gilles Delmaire, and Gilles Roussel


-----


In this paper, we consider the problem of blindly calibrating a mobile sensor network—i.e., determining the gain and the offset of each sensor—from heterogeneous observations on a defined spatial area over time. For that purpose, we propose to revisit blind sensor  calibration as an informed Nonnegative Matrix Factorization (NMF) problem with missing entries. In the considered framework, one matrix factor contains the calibration structure of the sensors—and especially the values of the sensed phenomenon—while the other  one contains the calibration parameters of the whole sensor network. The available information is taken into account by using a  specific parameterization of the NMF problem. Moreover, we also consider additional NMF constraints which can be independently taken  into account, i.e., an approximate constraint over the mean calibration parameters and a sparse approximation of the sensed  phenomenon over a known dictionary. The enhancement of our proposed approaches is investigated through more than 5000 simulations and is shown to be accurate for the considered application and to outperform a multi-hop micro-calibration technique as well as a method based on low-rank matrix completion and nonnegative least squares.

### Download
[Download the Matlab code of the mobile sensor calibration techniques](ToDo).
The code was written by Clément DORFFER and is maintained by Clément DORFFER, Matthieu PUIGT, Gilles DELMAIRE, and Gilles ROUSSEL (clement.dorffer [at] ensta-bretagne.fr and {matthieu.puigt, gilles.delmaire, gilles.roussel} [at] univ-littoral.fr).

It contains several functions and **folders**:

* README.m
* script_demo.m
* **functions**
    * ACIN_Cal.m
    * IN_Cal.m
    * OMP.m 
    * SpAIN_Cal.m
    * SpIN_Cal.m


### Reference
If you use this code for research or educational purpose, please cite:

> C. Dorffer, M. Puigt, G. Delmaire, G. Roussel, *"[Informed Nonnegative Matrix Factorization Methods for Mobile Sensor Network Calibration](https://hal.archives-ouvertes.fr/hal-01580604)"*, IEEE Transactions on Signal and Information Processing over Networks, Volume 4, Issue 4, pp. 667-682, December 2018.  
> 

### Support

For any suggestions or questions about this code, please contact: clement.dorffer [at] ensta-bretagne.fr and matthieu.puigt [at] univ-littoral.fr.